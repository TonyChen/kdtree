/*************************************************************************
 * Name:         Tony Chen
 * Email:        tonychen@finenet.com.tw
 *
 * Compilation:  javac PointSET.java
 * Execution:    java PointSET {input.txt}
 * Dependencies: Point2D.java RectHV.java StdDraw.java Queue.java
 *
 * Description: Brute-force implementation.
 * For a set of points in the unit square to implement range search (find all
 * of the points contained in a query rectangle) and nearest neighbor search
 * (find a closest point to a query point) by using a red-black BST .
 *************************************************************************/

import java.util.ArrayList;

public class PointSET {

    private RBTree tree;

    private static class RBTree {
        private static final boolean RED   = true;
        private static final boolean BLACK = false;

        private Node root;
        private int sz;

        private static class Node {
            private Point2D key;
            private Node left, right;
            private boolean color;

            Node(Point2D key) {
                this.key = key;
                this.color = RED;
            }
        }

        private boolean isRed(Node node) {
            return node != null && node.color == RED;
        }

        public int size() {
            return sz;
        }

        public Node search(Point2D key) {
            Node x = root;
            while (x != null) {
                int cmp = key.compareTo(x.key);
                if (cmp == 0) return x;
                else if (cmp < 0) x = x.left;
                else if (cmp > 0) x = x.right;
            }
            return null;
        }

        Node rotateLeft(Node h) {
            Node x = h.right;
            h.right = x.left;
            x.left = h;
            x.color = h.color;
            h.color = RED;
            return x;
        }

        Node rotateRight(Node h) {
            Node x = h.left;
            h.left = x.right;
            x.right = h;
            x.color = h.color;
            h.color = RED;
            return x;
        }

        void colorFlip(Node h) {
            h.color = !h.color;
            h.left.color = !h.left.color;
            h.right.color = !h.right.color;
        }

        public void insert(Point2D key) {
            root = insert(root, key);
            root.color = BLACK;
        }

        private Node insert(Node k, Point2D key) {
            if (k == null) {
                sz++;
                return new Node(key);
            }

            Node h = k;
            // move this line to the end to get 2-3 trees
            if (isRed(h.left) && isRed(h.right)) colorFlip(h);

            int cmp = key.compareTo(h.key);
            if (cmp < 0)      h.left  = insert(h.left, key);
            else if (cmp > 0) h.right = insert(h.right, key);

            if (isRed(h.right) && !isRed(h.left))    h = rotateLeft(h);
            if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);

            // -> move "if (isRed...) colorFlip(h)" to here to get 2-3 trees

            return h;
        }

        private Node moveRedLeft(Node k) {
            Node h = k;
            colorFlip(h);
            if (isRed(h.right.left)) {
                h.right = rotateRight(h.right);
                h = rotateLeft(h);
                colorFlip(h);
            }
            return h;
        }

        private Node moveRedRight(Node k) {
            Node h = k;
            colorFlip(h);
            if (isRed(h.left.left)) {
                h = rotateRight(h);
                colorFlip(h);
            }
            return h;
        }

        private Node fixUp(Node k) {
            Node h = k;
            // Fix right-learning red nodes
            if (isRed(h.right)) h = rotateLeft(h);

            // Detect if there is a 4-node that traverses down the left.
            // This is fixed by a right rotation, making both of the red
            // nodes the children of h.
            if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);

            // Split 4-node.
            if (isRed(h.left) && isRed(h.right)) colorFlip(h);

            return h;
        }

        private Node min(Node k) {
            Node h = k;
            while (h.left != null) {
                h = h.left;
            }
            return h;
        }

        public void deleteMin() {
            root = deleteMin(root);
            root.color = BLACK;
        }

        private Node deleteMin(Node k) {
            Node h = k;
            if (h.left == null) return null;

            if (!isRed(h.left) && !isRed(h.left.left)) h = moveRedLeft(h);
            h.left = deleteMin(h.left);

            return fixUp(h);
        }

        public void delete(Point2D key) {
            root = delete(root, key);
            root.color = BLACK;
        }

        private Node delete(Node k, Point2D key) {
            Node h = k;
            if (key.compareTo(h.key) < 0) {
                if (!isRed(h.left) && !isRed(h.left.left)) h = moveRedLeft(h);
                h.left = delete(h.left, key);
            } else {
                if (isRed(h.left)) h = rotateRight(h);
                if (key.compareTo(h.key) == 0 && h.right == null) return null;

                if (!isRed(h.right) && !isRed(h.right.left))
                    h = moveRedRight(h);

                if (key.compareTo(h.key) == 0) {
                    // h.val = search(h.right, min(h.right).key);
                    h.key = min(h.right).key;
                    h.right = deleteMin(h.right);
                } else {
                    h.right = delete(h.right, key);
                }
            }

            return fixUp(h);
        }

        // /* !!debug */
        // public void print(Node node, int level, String prefixed) {
        //     if (node != null) {
        //         print(node.right, level+1, "/");

        //         int sp = level * 20 + 1;
        //         StdOut.println(String.format(
        //             "%"+sp+"s%s", prefixed, node.key.toString()));

        //         print(node.left, level+1, "\\");
        //     }
        // }

        public void draw(Node node) {
            if (node != null) {
                draw(node.left);
                node.key.draw();
                draw(node.right);
            }
        }

        public ArrayList<Point2D> range(RectHV rect) {
            ArrayList<Point2D> result = new ArrayList<Point2D>();
            if (sz == 0) return result;

            Stack<Node> stack = new Stack<Node>();
            stack.push(root);

            double minX = rect.xmin();
            double maxX = rect.xmax();
            double minY = rect.ymin();
            double maxY = rect.ymax();

            while (!stack.isEmpty()) {
                Node node = stack.pop();
                Point2D p = node.key;

                double y = p.y();
                if (y <= maxY) {
                    if (node.right != null) stack.push(node.right);
                }
                if (y >= minY) {
                    if (node.left != null) stack.push(node.left);

                    if (y <= maxY) {
                        double x = p.x();
                        if (minX <= x && x <= maxX) {
                            result.add(p);
                        }
                    }
                }
            }

            return result;
        }

        public Point2D nearest(Point2D p0) {
            if (sz == 0) return null;

            Stack<Node> stack = new Stack<Node>();
            stack.push(root);

            Point2D result = null;
            double dist = Double.POSITIVE_INFINITY;
            double y0 = p0.y();

            while (!stack.isEmpty()) {
                Node node = stack.pop();
                Point2D p = node.key;

                double d = p0.distanceSquaredTo(p);
                if (d < dist) {
                    result = p;
                    if (d == 0) break;
                    dist = d;

                    if (node.right != null) stack.push(node.right);
                    if (node.left  != null) stack.push(node.left);
                } else if (node.left != null || node.right != null) {
                    d = p.y() - y0;
                    if (node.right != null) {
                        if (d <= 0 || d*d < dist) stack.push(node.right);
                    }
                    if (node.left != null) {
                        if (d >= 0 || d*d < dist) stack.push(node.left);
                    }
                }
            }

            return result;
        }
    }

    // construct an empty set of points
    public PointSET() {
        tree = new RBTree();
    }

    // is the set empty?
    public boolean isEmpty() {
        return tree.size() == 0;
    }

    // number of points in the set
    public int size() {
        return tree.size();
    }

    // add the point p to the set (if it is not already in the set)
    public void insert(Point2D p) {
        tree.insert(p);
    }

    // does the set contain the point p?
    public boolean contains(Point2D p) {
        return tree.search(p) != null;
    }

    // draw all of the points to standard draw
    public void draw() {
        tree.draw(tree.root);
    }

    // /* !!debug */
    // public void print() {
    //     tree.print(tree.root, 0, "=");
    // }

    // all points in the set that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        return tree.range(rect);
    }

    // a nearest neighbor in the set to p; null if set is empty
    public Point2D nearest(Point2D p) {
        return tree.nearest(p);
    }

    private static Point2D[] readPoints(String filename) {
        In in = new In(filename);
        String[] s = in.readAllStrings();

        int n = s.length / 2;
        Point2D[] result = new Point2D[n];

        for (int i = 0; i < n; i++) {
            double x = Double.parseDouble(s[2*i]);
            double y = Double.parseDouble(s[2*i+1]);
            result[i] = new Point2D(x, y);
        }
        return result;
    }

    public static void main(String[] args) {
        PointSET brute = new PointSET();

        if (args.length >= 1) {
            /* !!debug */StdOut.print("read points ... ");
            /* !!debug */Stopwatch t = new Stopwatch();
            Point2D[] points = readPoints(args[0]);
            /* !!debug */StdOut.println(String.format(
                "%d ... %gs", points.length, t.elapsedTime()));

            /* !!debug */StdOut.print("brute> insert points ... ");
            /* !!debug */t = new Stopwatch();
            for (Point2D p : points) {
                brute.insert(p);
            }
            /* !!debug */StdOut.println(t.elapsedTime()+"s");

            StdDraw.show(0);
            if (brute.size() <= 10000) StdDraw.setPenRadius(0.01);

            /* !!debug */StdOut.print("brute> draw ... ");
            /* !!debug */t = new Stopwatch();
            brute.draw();
            /* !!debug */StdOut.println(t.elapsedTime()+"s");
        }

        double x0 = 0;
        double y0 = 0;
        while (true) {
            StdDraw.show(50);
            if (StdDraw.mousePressed()) {
                double x = StdDraw.mouseX();
                double y = StdDraw.mouseY();
                if (x == x0 && y == y0) continue;
                x0 = x;
                y0 = y;
                if (0 <= x && x <= 1 && 0 <= y && y <= 1) {
                    Point2D p = new Point2D(x, y);
                    brute.insert(p);
                    StdDraw.clear();
                    brute.draw();
                    System.out.printf("%d: %8.6f %8.6f\n", brute.size(), x, y);
                }
            }
        }
    }
}