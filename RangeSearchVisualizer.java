/*************************************************************************
 *  Compilation:  javac RangeSearchVisualizer.java
 *  Execution:    java RangeSearchVisualizer input.txt
 *  Dependencies: PointSET.java KdTree.java Point2D.java RectHV.java
 *                StdDraw.java In.java
 *
 *  Read points from a file (specified as a command-line arugment) and
 *  draw to standard draw. Also draw all of the points in the rectangle
 *  the user selects by dragging the mouse.
 *
 *  The range search results using the brute-force algorithm are drawn
 *  in red; the results using the kd-tree algorithms are drawn in blue.
 *
 *************************************************************************/

public class RangeSearchVisualizer {

    private static Point2D[] readPoints(String filename) {
        In in = new In(filename);
        String[] s = in.readAllStrings();

        int n = s.length / 2;
        Point2D[] result = new Point2D[n];

        for (int i = 0; i < n; i++) {
            double x = Double.parseDouble(s[2*i]);
            double y = Double.parseDouble(s[2*i+1]);
            result[i] = new Point2D(x, y);
        }
        return result;
    }

    public static void main(String[] args) {
        Stopwatch t;
        PointSET brute = new PointSET();
        KdTree kdtree = new KdTree();
        {
            /* !!debug */StdOut.print("read points ... ");
            /* !!debug */t = new Stopwatch();
            Point2D[] points = readPoints(args[0]);
            /* !!debug */StdOut.println(String.format(
                "%d ... %gs", points.length, t.elapsedTime()));

            /* !!debug */StdOut.print("insert points into brute/kdtree... ");
            /* !!debug */t = new Stopwatch();
            for (Point2D p : points) {
                brute.insert(p);
                kdtree.insert(p);
            }
            /* !!debug */StdOut.println(t.elapsedTime()+"s");
        }

        double x0 = 0.0, y0 = 0.0;      // initial endpoint of rectangle
        double x1 = 0.0, y1 = 0.0;      // current location of mouse
        boolean isDragging = false;     // is the user dragging a rectangle
        boolean isRefresh = false;

        StdDraw.show(0);
        double penRadius = 0.002;       // 0.01
        if (brute.size() < 1000) penRadius = 0.01;

        /* !!debug */StdOut.print("init--> draw points ... ");
        /* !!debug */t = new Stopwatch();
        // draw the points
        StdDraw.clear();
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(penRadius);
        brute.draw();
        /* !!debug */StdOut.println(t.elapsedTime()+"s");

        while (true) {
            StdDraw.show(40);

            // user starts to drag a rectangle
            if (StdDraw.mousePressed() && !isDragging) {
                x0 = StdDraw.mouseX();
                y0 = StdDraw.mouseY();
                isDragging = true;
                continue;
            }

            // user is dragging a rectangle
            else if (StdDraw.mousePressed() && isDragging) {
                x1 = StdDraw.mouseX();
                y1 = StdDraw.mouseY();
                continue;
            }

            // mouse no longer pressed
            else if (!StdDraw.mousePressed() && isDragging) {
                isDragging = false;
                isRefresh = true;
            }

            if (!isRefresh) continue;
            isRefresh = false;

            RectHV rect = new RectHV(Math.min(x0, x1), Math.min(y0, y1),
                                     Math.max(x0, x1), Math.max(y0, y1));

            /* !!debug */StdOut.print("------> draw points ... ");
            /* !!debug */t = new Stopwatch();
            // draw the points
            StdDraw.clear();
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(penRadius);
            brute.draw();
            StdDraw.show(0);
            /* !!debug */StdOut.println(t.elapsedTime()+"s");

            // draw the rectangle
            StdDraw.setPenColor(StdDraw.MAGENTA);
            StdDraw.setPenRadius();
            rect.draw();
            StdDraw.show(0);

            /* !!debug */StdOut.print("brute > range ... ");
            /* !!debug */t = new Stopwatch();
            // draw the range search results for brute-force data structure in red
            StdDraw.setPenRadius(3*penRadius);
            StdDraw.setPenColor(StdDraw.RED);
            for (Point2D p : brute.range(rect))
                p.draw();
            StdDraw.show(0);
            /* !!debug */StdOut.println(t.elapsedTime()+"s");

            /* !!debug */StdOut.print("kdtree> range ... ");
            /* !!debug */t = new Stopwatch();
            // draw the range search results for kd-tree in blue
            StdDraw.setPenRadius(2*penRadius);
            StdDraw.setPenColor(StdDraw.BLUE);
            for (Point2D p : kdtree.range(rect))
                p.draw();
            StdDraw.show(0);
            /* !!debug */StdOut.println(t.elapsedTime()+"s");
        }
    }
}
