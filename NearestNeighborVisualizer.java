/*************************************************************************
 *  Compilation:  javac NearestNeighborVisualizer.java
 *  Execution:    java NearestNeighborVisualizer input.txt
 *  Dependencies: PointSET.java KdTree.java Point2D.java In.java StdDraw.java
 *
 *  Read points from a file (specified as a command-line argument) and
 *  draw to standard draw. Highlight the closest point to the mouse.
 *
 *  The nearest neighbor according to the brute-force algorithm is drawn
 *  in red; the nearest neighbor using the kd-tree algorithm is drawn in blue.
 *
 *************************************************************************/

public class NearestNeighborVisualizer {

    private static Point2D[] readPoints(String filename) {
        In in = new In(filename);
        String[] s = in.readAllStrings();

        int n = s.length / 2;
        Point2D[] result = new Point2D[n];

        for (int i = 0; i < n; i++) {
            double x = Double.parseDouble(s[2*i]);
            double y = Double.parseDouble(s[2*i+1]);
            result[i] = new Point2D(x, y);
        }
        return result;
    }

    public static void main(String[] args) {
        Stopwatch t;
        PointSET brute = new PointSET();
        KdTree kdtree = new KdTree();
        {
            /* !!debug */StdOut.print("read points ... ");
            /* !!debug */t = new Stopwatch();
            Point2D[] points = readPoints(args[0]);
            /* !!debug */StdOut.println(String.format(
                "%d ... %gs", points.length, t.elapsedTime()));

            /* !!debug */StdOut.print("insert points into brute/kdtree... ");
            /* !!debug */t = new Stopwatch();
            for (Point2D p : points) {
                brute.insert(p);
                kdtree.insert(p);
            }
            /* !!debug */StdOut.println(t.elapsedTime()+"s");
        }

        boolean isShowAll = false;
        int refreshLimit = 50000;
        if (args.length > 1) {
            int n = 1;
            if (args[n].length() == 1) {
                isShowAll = !args[n].equals("0");
                n++;
            }
            if (args.length > n) refreshLimit = Integer.parseInt(args[n]);
        }

        StdDraw.show(0);
        double penRadius = 0.002;
        if (brute.size() < 1000) penRadius = 0.01;

        double x0 = -1;
        double y0 = -1;
        boolean refresh = true;
        boolean isMousePressed = false;
        while (true) {
            if (!isMousePressed && StdDraw.mousePressed()) {
                isMousePressed = true;
            }
            if (isMousePressed && !StdDraw.mousePressed()) {
                isMousePressed = false;
                isShowAll = !isShowAll;
                if (isShowAll)
                    StdOut.println("Show different for point & distance");
                else
                    StdOut.println("Show different distance only");
            }

            // the location (x, y) of the mouse
            double x = StdDraw.mouseX();
            double y = StdDraw.mouseY();
            if (x == x0 && y == y0) {
                StdDraw.show(40);
                continue;
            }
            x0 = x;
            y0 = y;

            Point2D query = new Point2D(x, y);

            if (refresh) {
                /* !!debug */StdOut.print("------> draw points ... ");
                /* !!debug */t = new Stopwatch();
                // draw all of the points
                StdDraw.clear();
                StdDraw.setPenColor(StdDraw.BLACK);
                StdDraw.setPenRadius(penRadius);
                brute.draw();
                /* !!debug */StdOut.println(t.elapsedTime()+"s");
                refresh = (brute.size() < refreshLimit);
            }

            // draw in red the nearest neighbor (using brute-force algorithm)
            StdDraw.setPenRadius(0.03);
            StdDraw.setPenColor(StdDraw.RED);
            /* !!debug */StdOut.print("brute > nearest ... ");
            /* !!debug */t = new Stopwatch();
            Point2D p1 = brute.nearest(query);
            /* !!debug */StdOut.println(t.elapsedTime()+"s");
            p1.draw();

            // draw in blue the nearest neighbor (using kd-tree algorithm)
            StdDraw.setPenRadius(0.02);
            StdDraw.setPenColor(StdDraw.BLUE);
            /* !!debug */StdOut.print("kdtree> nearest ... ");
            /* !!debug */t = new Stopwatch();
            Point2D p2 = kdtree.nearest(query);
            /* !!debug */StdOut.println(t.elapsedTime()+"s");
            p2.draw();
            StdDraw.show(0);

            if (p1.compareTo(p2) != 0) {
                double d1 = p1.distanceTo(query);
                double d2 = p2.distanceTo(query);
                if (isShowAll || d1 != d2) {
                    String msg = "Point";
                    if (d1 != d2) msg = "Error";
                    StdOut.println(String.format(
                        "%s> %s B%s:%g K%s:%g",
                        msg, query.toString(),
                        p1.toString(), d1,
                        p2.toString(), d2
                    ));
                }
            }

            StdDraw.show(40);
        }
    }
}
