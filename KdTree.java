/*************************************************************************
 * Name:         Tony Chen
 * Email:        tonychen@finenet.com.tw
 *
 * Compilation:  javac KdTree.java
 * Execution:    java KdTree {input.txt}
 * Dependencies: Point2D.java RectHV.java StdDraw.java Queue.java
 *
 * Description: A mutable data type that uses a 2d-tree to represent a set of
 * points in the unit square. A 2d-tree is a generalization of a BST to
 * two-dimensional keys. The idea is to build a BST with points in the nodes,
 * using the x and y coordinates of the points as keys in strictly alternating
 * sequence. The prime advantage of a 2d-tree over a BST is that it supports
 * efficient implementation of range search and nearest neighbor search. Each
 * node corresponds to an axis-aligned rectangle in the unit square, which
 * encloses all of the points in its subtree. The root corresponds to the unit
 * square; the left and right children of the root corresponds to the two
 * rectangles split by the x-coordinate of the point at the root; and so
 * forth.
 *************************************************************************/

import java.util.ArrayList;

public class KdTree {

    private static final double PEN_RADIUS = 0.01;

    private Node root;
    private int sz;
    private double penRadius = PEN_RADIUS;

    private static class Node {
        private Point2D p;      // the point
        private Node lb;        // the left/bottom subtree
        private Node rt;        // the right/top subtree

        public Node(Point2D pt) { p = pt; }
        public double x() { return p.x(); }
        public double y() { return p.y(); }
        public void draw() { p.draw(); }

        // return < 0 : pt in left/bottom of p
        // return = 0 : pt == p
        // return > 0 : pt in right/top of p
        public int compare(Point2D pt, int level) {
            int cmp;
            if ((level & 1) == 0) {
                cmp = Double.compare(pt.x(), p.x());
                if (cmp == 0) cmp = Double.compare(pt.y(), p.y());
            } else {
                cmp = Double.compare(pt.y(), p.y());
                if (cmp == 0) cmp = Double.compare(pt.x(), p.x());
            }
            return cmp;
        }
    }

    private static class NodeLevel {
        protected Node node;    // the node: point / lb node / rt node
        protected int level;    // even: vertical split, odd: horizontal split

        public NodeLevel(Node node, int level) {
            this.node = node;
            this.level = level;
        }

        public Point2D point() { return node.p; }

        public double distanceSquaredToNode(Point2D pt) {
            return node.p.distanceSquaredTo(pt);
        }

        // pt vs node.p
        public int compare(Point2D pt) {
            return node.compare(pt, level);
        }

        // return left/bottom NodeLevel if rect on left/bottom side
        public NodeLevel nextLB(RectHV rect) {
            NodeLevel result = null;
            if (node.lb != null) {
                if ((level & 1) == 0) {
                    // vertical: left
                    if (rect.xmin() <= node.x())
                        result = new NodeLevel(node.lb, level+1);
                } else {
                    // horizontal: bottom
                    if (rect.ymin() <= node.y())
                        result = new NodeLevel(node.lb, level+1);
                }
            }
            return result;
        }

        // return right/top NodeLevel if rect on right/top side
        public NodeLevel nextRT(RectHV rect) {
            NodeLevel result = null;
            if (node.rt != null) {
                if ((level & 1) == 0) {
                    // vertical: right
                    if (rect.xmax() >= node.x())
                        result = new NodeLevel(node.rt, level+1);
                } else {
                    // horizontal: top
                    if (rect.ymax() >= node.y())
                        result = new NodeLevel(node.rt, level+1);
                }
            }
            return result;
        }
    }

    private static class NodeSpace extends NodeLevel {
        private RectHV rect;    // space contains the node

        public NodeSpace(Node node, RectHV rect, int level) {
            super(node, level);
            this.rect = rect;
        }

        public void draw(double penRadius) {
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(penRadius);
            node.draw();
            StdDraw.setPenRadius();

            if ((level & 1) == 0) {
                // vertical: left | right
                double x = node.x();
                StdDraw.setPenColor(StdDraw.RED);
                StdDraw.line(x, rect.ymin(), x, rect.ymax());
            } else {
                // horizontal: bottom - top
                double y = node.y();
                StdDraw.setPenColor(StdDraw.BLUE);
                StdDraw.line(rect.xmin(), y, rect.xmax(), y);
            }
        }

        public void draw() {
            draw(PEN_RADIUS);
        }

        public double distanceSquaredToRect(Point2D pt) {
            return rect.distanceSquaredTo(pt);
        }

        // return NodeSpace of left/bottom
        public NodeSpace nextLB() {
            if (node.lb == null) return null;
            RectHV h;
            if ((level & 1) == 0) {
                // vertical: left
                h = new RectHV(rect.xmin(), rect.ymin(),
                               node.x()   , rect.ymax());
            } else {
                // horizontal: bottom
                h = new RectHV(rect.xmin(), rect.ymin(),
                               rect.xmax(), node.y());
            }
            return new NodeSpace(node.lb, h, level+1);
        }

        // return NodeSpace of right/top
        public NodeSpace nextRT() {
            if (node.rt == null) return null;
            RectHV h;
            if ((level & 1) == 0) {
                // vertical: right
                h = new RectHV(node.x()   , rect.ymin(),
                               rect.xmax(), rect.ymax());
            } else {
                // horizontal: top
                h = new RectHV(rect.xmin(), node.y()   ,
                               rect.xmax(), rect.ymax());
            }
            return new NodeSpace(node.rt, h, level+1);
        }
    }

    // is the set empty?
    public boolean isEmpty() {
        return sz == 0;
    }

    // number of points in the set
    public int size() {
        return sz;
    }

    // add the point p to the set (if it is not already in the set)
    public void insert(Point2D p) {
        if (root == null) {
            root = new Node(p);
            sz = 1;
            return;
        }

        Node h = root;
        int level = 0;

        for (;;) {
            int cmp = h.compare(p, level);
            if (cmp == 0) return;
            if (cmp < 0) {
                if (h.lb == null) {
                    h.lb = new Node(p);
                    sz++;
                    return;
                }
                h = h.lb;
            } else {
                if (h.rt == null) {
                    h.rt = new Node(p);
                    sz++;
                    return;
                }
                h = h.rt;
            }

            level++;
        }
    }

    // does the set contain the point p?
    public boolean contains(Point2D p) {
        Node h = root;
        int level = 0;

        while (h != null) {
            int cmp = h.compare(p, level);
            if (cmp == 0) return true;
            if (cmp < 0) h = h.lb;
            else         h = h.rt;
            level++;
        }

        return false;
    }

    // draw kd-chart to standard draw
    public void draw() {
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.rectangle(0.5, 0.5, 0.5, 0.5);
        if (root == null) return;

        Stack<NodeSpace> stack = new Stack<NodeSpace>();
        stack.push(new NodeSpace(root, new RectHV(0, 0, 1, 1), 0));

        while (!stack.isEmpty()) {
            NodeSpace ns = stack.pop();
            ns.draw(penRadius);

            NodeSpace ns2 = ns.nextRT();
            if (ns2 != null) stack.push(ns2);

            NodeSpace ns1 = ns.nextLB();
            if (ns1 != null) stack.push(ns1);
        }
    }

    // all points in the set that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {
        ArrayList<Point2D> result = new ArrayList<Point2D>();

        if (root != null) {
            Stack<NodeLevel> stack = new Stack<NodeLevel>();
            stack.push(new NodeLevel(root, 0));

            while (!stack.isEmpty()) {
                NodeLevel nodeLVL = stack.pop();
                Point2D p = nodeLVL.point();

                if (rect.contains(p)) result.add(p);

                NodeLevel nextLVL = nodeLVL.nextRT(rect);
                if (nextLVL != null) stack.push(nextLVL);

                nextLVL = nodeLVL.nextLB(rect);
                if (nextLVL != null) stack.push(nextLVL);
            }
        }

        return result;
    }

    // a nearest neighbor in the set to p; null if set is empty
    public Point2D nearest(Point2D p) {
        if (sz == 0) return null;

        Stack<NodeSpace> stack = new Stack<NodeSpace>();
        stack.push(new NodeSpace(root, new RectHV(0, 0, 1, 1), 0));

        Point2D result = null;
        double distSquared = Double.POSITIVE_INFINITY;

        while (!stack.isEmpty()) {
            NodeSpace ns = stack.pop();
            if (ns.distanceSquaredToRect(p) >= distSquared) continue;

            double d = ns.distanceSquaredToNode(p);
            if (d < distSquared) {
                result = ns.point();
                if (d == 0) break;
                distSquared = d;
            }

            NodeSpace ns1 = ns.nextLB();
            NodeSpace ns2 = ns.nextRT();
            if (ns1 != null && ns2 != null) {
                if (ns.compare(p) <= 0) {
                    // p in Left/Bottom of ns.node
                    stack.push(ns2);    // Right/Top last
                    stack.push(ns1);    // Left/Bottom first
                } else {
                    // p in Right/Top of ns.node
                    stack.push(ns1);    // Left/Bottom last
                    stack.push(ns2);    // Right/Top first
                }
            } else {
                if (ns2 != null) stack.push(ns2);
                if (ns1 != null) stack.push(ns1);
            }
        }

        return result;
    }

    private static Point2D[] readPoints(String filename) {
        In in = new In(filename);
        String[] s = in.readAllStrings();

        int n = s.length / 2;
        Point2D[] result = new Point2D[n];

        for (int i = 0; i < n; i++) {
            double x = Double.parseDouble(s[2*i]);
            double y = Double.parseDouble(s[2*i+1]);
            result[i] = new Point2D(x, y);
        }
        return result;
    }

    public static void main(String[] args) {
        KdTree kdtree = new KdTree();

        if (args.length >= 1) {
            /* !!debug */StdOut.print("read points ... ");
            /* !!debug */Stopwatch t = new Stopwatch();
            Point2D[] points = readPoints(args[0]);
            /* !!debug */StdOut.println(String.format(
                "%d ... %gs", points.length, t.elapsedTime()));

            /* !!debug */StdOut.print("brute> insert points ... ");
            /* !!debug */t = new Stopwatch();
            for (Point2D p : points) {
                kdtree.insert(p);
            }
            /* !!debug */StdOut.println(t.elapsedTime()+"s");

            StdDraw.show(0);
            if (kdtree.size() >= 1000) kdtree.penRadius = PEN_RADIUS / 2;

            /* !!debug */StdOut.print("kdtree> draw ... ");
            /* !!debug */t = new Stopwatch();
            kdtree.draw();
            /* !!debug */StdOut.println(t.elapsedTime()+"s");
        }

        double x0 = 0;
        double y0 = 0;
        while (true) {
            StdDraw.show(50);
            if (StdDraw.mousePressed()) {
                double x = StdDraw.mouseX();
                double y = StdDraw.mouseY();
                if (x == x0 && y == y0) continue;
                x0 = x;
                y0 = y;
                if (0 <= x && x <= 1 && 0 <= y && y <= 1) {
                    Point2D p = new Point2D(x, y);
                    kdtree.insert(p);
                    StdDraw.clear();
                    kdtree.draw();
                    System.out.printf("%d: %8.6f %8.6f\n", kdtree.size(), x, y);
                }
            }
        }
    }

}