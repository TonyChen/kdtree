# Makefile
# vim: set noet ts=8 sw=8:

SOURCE=PointSET.java KdTree.java
TARGET=$(SOURCE:.java=.class)

.PHONY: all compile clean work test help checkstyle findbugs zip visual

%.class: %.java
	@/bin/echo -n "% "
	javac-algs4 $<

compile: $(TARGET)
	@echo "Compiling done."

all: test work
	@echo "All done."

work: checkstyle findbugs zip

clean:
	rm -f *.class *.zip

checkstyle: $(SOURCE)
	@/bin/echo -n "% "
	checkstyle-algs4 $^

findbugs: $(SOURCE:.java=.class)
	@/bin/echo -n "% "
	findbugs-algs4 *.class

help:
	@echo "usage: make [all | checkstyle | findbugs | zip | test]"

zip: kdtree.zip

kdtree.zip: $(SOURCE)
	@/bin/echo -n "% "
	zip -u $@ $^

test: $(TARGET) visual
	@/bin/echo -n "% "
	java-algs4 KdTree data/circle10.txt
	@/bin/echo -n "% "
	java-algs4 -Xmx1600m RangeSearchVisualizer data/input10K.txt
	@/bin/echo -n "% "
	java-algs4 -Xmx1600m NearestNeighborVisualizer data/input1M.txt

visual: $(TARGET) \
	NearestNeighborVisualizer.class RangeSearchVisualizer.class
